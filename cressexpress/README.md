# CressExpress Wildfly #
This directory holds configuration files for the Wildfly web application Server.
### Information ###
1. standalone.xml contains the database configurations and email configuration required for Cressexpress Web Application.
2. Other standalone-.xml were generated during the process of migrating Wildfly from version 8 to 11. However, they do not contains any custom configurations related to cressexpress.
3. Coexpression war file and required jdk will be downloaded and placed in this folder when cressexpress.sh is executed. Note: User is not supposed to add these files to git repository.
**NOTE**: To use your own war file for deplyoment. Execute cressexpress.sh and then replace coexpression-1.0.war with your war file.
### Standalone.xml Important configurations ###
1. Datasource-database configuration
![](https://bitbucket.org/lorainelab/cressexpress-docker/downloads/Wildfly_datasource_config.PNG)
2. Mail configuration
![](https://bitbucket.org/lorainelab/cressexpress-docker/downloads/Wildfly_mail_config.PNG)
