#!/bin/bash

echo "Enter RAM in GB (e.g., enter 16 for 16 GB)"
read ram
function valid_ip()
{
	local  ip=$1
	local  stat=1
	if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		OIFS=$IFS
		IFS='.'
		ip=($ip)
		IFS=$OIFS
		[[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
			&& ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
		stat=$?
	fi
	return $stat
}

echo "Enter hostname for Apache server configuration and CressExpress email links"
echo "a)localhost" 
echo "b)Custom domain (example.com)"
echo "c)IP address"
echo "Type a/b/c" 
read  val

if [ $val == b ]
then
	echo "Please enter your custom domain name"
	read hostname
#	host $hostname 2>&1 > /dev/null
#	if [[ $? -eq 0 ]]
#	then 
#		echo "Hostname set to $hostname"
#	else
#		echo "Can't resolve $hostname"
#		exit 1
#	fi 
elif [ $val == a ]
then
	hostname="localhost"
	echo "Hostname set to $hostname"
elif [ $val == c ]
then
	echo  "Please enter IP address of the host"
	read hostname
	valid_ip $hostname
	if [[ $? -eq 0 ]]
	then 
		echo "Hostname set to $hostname"
	else
		echo "Please input valid ip address"
		exit 1
	fi
else
	echo "Invalid Input.Please select one option from a/b/c."
	exit 1
fi

cp backup/docker-compose.yml .
cp backup/httpd.conf apache/conf/

datafilename="cressexpress-data-2.0.0.tar.gz"
datafilechecksum="e5135871730a16b1a828b491c349cf69"
dataurl="http://lorainelab-quickload.scidas.org/cressexpress/$datafilename"

databasefilename="cressexpress-database-2.0.0.tar.gz"
databasefilechecksum="4fa50066f4b1151b26d1e2ad08c6deef"
databaseurl="http://lorainelab-quickload.scidas.org/cressexpress/$databasefilename"

cressexpresswarfilename="coexpression-1.0.war"
warurl="https://bitbucket.org/lorainelab/cressexpress/downloads/$cressexpresswarfilename"


if [ -e $datafilename ]
then
	rm $datafilename
fi
if [ -e $databasefilename ]
then
	rm $databasefilename
fi

if ! [ -e ./cressexpress/$cressexpresswarfilename ]
then 
	wget $warurl
	mv -v $cressexpresswarfilename cressexpress/
else
	echo "$cressexpresswarfilename already present in cressexpress/ folder."
fi

echo "Fetching Cressexpress Data"


wget $dataurl
md5data=`md5sum $datafilename | awk '{ print $1 }'`
if ! [ $md5data == $datafilechecksum ]
then 
	echo Md5 of downloaded file: $md5data
	echo Expected Md5: $datafilechecksum
	echo "Broken file! Stopping execution..........."
	rm $datafilename
	exit 1
fi
echo "fetching completed"
echo "================================================================================================================================"
echo "Uncompressing $datafilename"
tar xvzf $datafilename
mv -v data/env/wildfly/jdk/jdk-8u111-linux-x64.tar.gz cressexpress/
echo "Uncompressing finished"
chmod 777 -R data/
echo "================================================================================================================================"
echo "Fetching Cressexpress Database"


wget $databaseurl
md5database=`md5sum $databasefilename | awk '{ print $1 }'`
if ! [ $md5database == $databasefilechecksum ]
then 
	echo Md5 of downloaded file: $md5database
	echo Expected Md5: $databasefilechecksum
	echo "Broken file! Stopping execution..........."
	rm $databasefilename
	exit 1
fi
echo "fetching completed"
echo "================================================================================================================================"
echo "Uncompressing $databasefilename"
tar xvzf $databasefilename -C mysql/
echo "Uncompressing finished"
echo "================================================================================================================================"
echo "Cleaning started"
rm $databasefilename
rm $datafilename
echo "Cleaning finished"
sed -i "s/_RAM_/$ram/" docker-compose.yml
sed -i "s/_HOST_NAME_VARIABLE_/$hostname/" docker-compose.yml
sed -i "s/_HOST_NAME_VARIABLE_/$hostname/" apache/conf/httpd.conf
echo "success!!"
