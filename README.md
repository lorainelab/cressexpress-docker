# CressExpress Web Application Dockerized setup #
------
This repository contains files required to create CressExpress setup using docker-compose. This approach will save tedious job of configuring different components of cressexpress.

**Note:** CressExpress war file is downloaded from [CressExpress main repository](https://bitbucket.org/lorainelab/cressexpress).

## Prerequisites ##
------
* Storage: 20 GB min 
* System Memory: depends on load; 64 Gb recommended, 12 Gb bare minimum
* [Docker](https://www.docker.com/community-edition#/download) (if running on ***MAC/Windows*** assign min 12GB of RAM to virtual machine used by Docker: Docker settings-> Advanced -> Memory)
* [Docker-compose](https://docs.docker.com/compose/install/)
* [Git](https://git-scm.com/downloads)

## Set up instructions - AWS EC2 (CentOS) ##
------
1. Install git, docker: `sudo yum -y install git docker`
2. Start Docker deamon: `sudo service docker start` 
3. Install docker compose. Make it executable. (Tested with version 1.21.2)

```
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` > docker-compose
chmod +x docker-compose
sudo chown root docker-compose
sudo chgrp root docker-compose
sudo mv docker-compose /usr/local/bin/.
```

## Set up instructions - Windows ##
------
1. If you are using Windows platform please follow these steps first
	* Open git-bash
    * type following command `git config --global core.autocrlf false`
2. Proceed to next section

## Set up instructions - All ##
------    
1. Clone this repository to your machine
2. Open terminal ([bash](https://docs.microsoft.com/en-us/windows/wsl/install-win10) for Windows) inside the local copy
3. Run `cressexpress.sh` 
4. Follow on-screen instruction
    * Provide **RAM** size (should be greater than equal to 12 GB)
    * Provide **hostname** required for Apache and email links
        * Email contains links of results generated after analysis. This hostname is essential for users to download results.
        * Select one of three options
            1. ***localhost***: if you are running cressexpress on your machine
            2. ***Custom domain***: if you have your personal domain setup 
            3. ***IP Address***: if you dont have custom domain but this setup executed on remote host (for eg AWS EC2)
5. Wait for the success message

## Execution instructions ##
------
1. Open terminal (unix/linux) or CMD/Powershell (Windows) inside the local copy
2. Run `sudo docker-compose up` 
3. Wait till it builds all docker containers (cressexpress_apache, cressexpress_wildfly, cressexpress_mysql)
4. Messages will be printed to the screen, untill the following:
![](https://bitbucket.org/lorainelab/cressexpress-docker/downloads/build_success.PNG)
5. Open web browser and visit the host name you entered in the previous step. 

## Managing CressExpress containers using docker and docker-compose ##
------
1. To start already existing containers:
    * Go to the local copy: `docker-compose start`
2. Stop already running containers:
    * Go to the local copy: `docker-compose stop`
3. Remove the continers:
    * Go to the local copy, stop the containers, then run: `docker-compose rm`
4. Get a list of running containers: `docker ls`
    * `docker exec -it [container id] bash`
5. View and query the CressExpress database
    * enter the container
    * launch mysql client: `mysql -u root -p cressexpress`
    * at mysql prompt, view available databases: `show database`
    * check jobs table: `use cressexpress; select email from jobs`
6. Copy a files to and from a container
    * from a container to current directory: `docker cp [container id]:[filename.txt] .`

## Source Code guidelines ##
------
1. [apache](https://bitbucket.org/lorainelab/cressexpress-docker/src/47ce707e2cf52e89c0b40949f40bdc60913a8b1f/apache/?at=master)
2. [Backup](https://bitbucket.org/lorainelab/cressexpress-docker/src/master/backup/)
    * This contains base httpd.conf and docker-compose.yml. These files have placeholders for some custom configuration.
2. [cressexpress](https://bitbucket.org/lorainelab/cressexpress-docker/src/bfc4aae8436a88d7c56324121a4bcfb65d9cd7d4/cressexpress/?at=master) (if you want to run your war file check this)
3. [mysql](https://bitbucket.org/lorainelab/cressexpress-docker/src/47ce707e2cf52e89c0b40949f40bdc60913a8b1f/mysql/?at=master)
4. [cressexpress.sh](https://bitbucket.org/lorainelab/cressexpress-docker/src/5056faf89ec64a25cdee11216c93a56a80823810/cressexpress.sh?at=master&fileviewer=file-view-default)
    * This file rquired to be executed before running docker-compose up
    * It does the job of prepopolating some required folders and database flat files required for the cressexpress
    * Following image has details imformation related to all commands and their purpose
        1. Getting latest war file from bitbukcet pipelines of cressexpress (wget)
        2. Getting data files from remote host (wget)
            * ***Data*** folder is fetched in form of .tar.gz file and then decompressed into data folder
            * The data folder contains following files:
            * ***env/data/*** -> *Results are stored in /results folderonce analysis is done. This folder also contains few file required for analysis 'exprRowData.txt*
            * ***env/lucene*** -> *This folder is used to stored indexs related to analysis*
            * ***env/wildfly*** -> *This folder contains files required for wildlfy server Jdk and java mysql driver*
            * Note: If you want to move your insfrastructure to a new host machine, Please back up this folder if you want to store the analysis results. And replace it with with data folder on new host.
        3. Getting flat database file from remote host (wget)
            * ***Database flat*** files are fetched in form of .tar.gz file and then decompressed unde rmysql folder as var and etc subfolders.
            * ***var/lib/cressexpress/*** -> *contains all flat database files.*
        4. Other commands are used for error handling and data validations
5. [docker-compose.yml](https://bitbucket.org/lorainelab/cressexpress-docker/src/master/backup/docker-compose.yml)